package AnsibleAAP

// Org -
type Org struct {
	id    int         `json:"id,omitempty"`
	items []Organisation `json:"items,omitempty"`
}


type OrgItem struct {
	items []Organisation `json:"items,omitempty"`
}

// Organisation -
type Organisation struct {
	id          		int              `json:"id"`
	Type        		string           `json:"type"`
	url 	   			string           `json:"url"`
	related				[]Related        `json:"related"`
	summary_fields 		[]Summary_Fields `json:"summary_fields"`
	created     		string           `json:"created"`
	modified     		string           `json:"modified"`
	name     			string           `json:"name"`
	description     	string           `json:"description"`
	max_hosts     		int              `json:"max_hosts"`
	custom_virtualenv  	string           `json:"custom_virtualenv"`
	default_environment	string           `json:"default_environment"`
}

type Related struct {
	created_by        					string   `json:"created_by"`
	modified_by        					string   `json:"modified_by"`
	execution_environments        		string   `json:"execution_environments"`
	projects        					string   `json:"projects"`
	inventories        					string   `json:"inventories"`
	job_templates        				string   `json:"job_templates"`
	workflow_job_templates        		string   `json:"workflow_job_templates"`
	users        						string   `json:"users"`
	admins        						string   `json:"admins"`
	teams        						string   `json:"teams"`
	credentials        					string   `json:"credentials"`
	applications        				string   `json:"applications"`
	activity_stream        				string   `json:"activity_stream"`
	notification_templates        		string   `json:"notification_templates"`
	notification_templates_started      string   `json:"notification_templates_started"`
	notification_templates_success      string   `json:"notification_templates_success"`
	notification_templates_error        string   `json:"notification_templates_error"`
	notification_templates_approvals	string   `json:"notification_templates_approvals"`
	object_roles        				string   `json:"object_roles"`
	access_list        					string   `json:"access_list"`
	instance_groups        				string   `json:"instance_groups"`
	galaxy_credentials        			string   `json:"galaxy_credentials"`
}

// Summary_Fields -
type Summary_Fields struct {
	created_by           []Created_By  `json:"created_by"`
	modified_by          []Modified_By `json:"modified_by"`
	object_roles         []Object_roles `json:"object_roles"`
	user_capabilities    []User_Capabilities `json:"user_capabilities"`
	related_field_counts []Related_Field_Count `json:"related_field_counts"`
}

type Created_By struct {
	id          int     `json:"id"`
	username    string  `json:"username"`
	first_name  string  `json:"first_name"`
	last_name	string  `json:"last_name"`
}

type Modified_By struct {
	id          int     `json:"id"`
	username    string  `json:"username"`
	first_name  string  `json:"first_name"`
	last_name	string  `json:"last_name"`
}

// Object_roles  -
type Object_roles struct {
	admin_role                        []Object_User_Only_Role	`json:"admin_role"`
	execute_role                      []Object_User_Role   		`json:"execute_role"`
	project_admin_role                []Object_User_Role  		`json:"project_admin_role"`
	inventory_admin_role              []Object_User_Role  		`json:"inventory_admin_role"`
	credential_admin_role             []Object_User_Role   		`json:"credential_admin_role"`
	workflow_admin_role               []Object_User_Role  		`json:"workflow_admin_role"`
	notification_admin_role           []Object_User_Role 		`json:"notification_admin_role`
	job_template_admin_role           []Object_User_Role 		`json:"job_template_admin_role`
	execution_environment_admin_role  []Object_User_Role  		`json:"execution_environment_admin_role`
	auditor_role                      []Object_User_Role 		`json:"auditor_role`
	member_role                       []Object_User_Only_Role 	`json:"member_role`
	read_role                         []Object_User_Role 		`json:"read_role`
	approval_role                     []Object_User_Role 		`json:"approval_role`
}

type Object_User_Only_Role  struct {
	description	string  `json:"description"`
	name    	string  `json:"name"`
	id    		int  	`json:"id"`
	user_only  	bool  	`json:"user_only"`
}

type Object_User_Role   struct {
	description	string  `json:"description"`
	name    	string  `json:"name"`
	id    		int  	`json:"id"`
}

type User_Capabilities struct {
	edit    bool  `json:"edit"`
	delete  bool  `json:"delete"`
}

type Related_Field_Count struct {
	inventories   int `json:"inventories`
	teams         int `json:"teams`
	users         int `json:"users`
	job_templates int `json:"job_templates`
	admins        int `json:"admins`
	projects      int `json:"projects"`
}

