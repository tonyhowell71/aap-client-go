package AnsibleAAP

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
)

// GetAllOrg - Returns all Ansible AAP Organisations
func (c *Client) GetAllOrgs(authToken *string) (*[]Org, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/api/v2/organizations", c.HostURL), nil)
	if err != nil {
		return nil, err
	}

	body, err := c.doRequest(req, authToken)
	if err != nil {
		return nil, err
	}

	orgs := []Org{}
	err = json.Unmarshal(body, &orgs)
	if err != nil {
		return nil, err
	}

	return &orgs, nil
}

// GetOrg - Returns a specific Ansible AAP Organisation
func (c *Client) GetOrg(orgID string, authToken *string) (*Org, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/api/v2/organizations/%s", c.HostURL, orgID), nil)
	if err != nil {
		return nil, err
	}

	body, err := c.doRequest(req, authToken)
	if err != nil {
		return nil, err
	}

	org := Org{}
	err = json.Unmarshal(body, &org)
	if err != nil {
		return nil, err
	}

	return &org, nil
}

// CreateOrg - Create new Ansible AAP Organisation
func (c *Client) CreateOrg(orgItems []OrgItem, authToken *string) (*Org, error) {
	rb, err := json.Marshal(orgItems)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", fmt.Sprintf("%s/api/v2/organizations/", c.HostURL), strings.NewReader(string(rb)))
	if err != nil {
		return nil, err
	}

	body, err := c.doRequest(req, authToken)
	if err != nil {
		return nil, err
	}

	org := Org{}
	err = json.Unmarshal(body, &org)
	if err != nil {
		return nil, err
	}

	return &org, nil
}

// UpdateOrg - Updates an Ansible AAP Organisation
func (c *Client) UpdateOrg(orgID string, orgItems []OrgItem, authToken *string) (*Org, error) {
	rb, err := json.Marshal(orgItems)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("PUT", fmt.Sprintf("%s/api/v2/organizations/%s", c.HostURL, orgID), strings.NewReader(string(rb)))
	if err != nil {
		return nil, err
	}

	body, err := c.doRequest(req, authToken)
	if err != nil {
		return nil, err
	}

	org := Org{}
	err = json.Unmarshal(body, &org)
	if err != nil {
		return nil, err
	}

	return &org, nil
}

// DeleteOrg - Deletes an Ansible AAP Organisation
func (c *Client) DeleteOrg(orgID string, authToken *string) error {
	req, err := http.NewRequest("DELETE", fmt.Sprintf("%s/api/v2/organizations/%s", c.HostURL, orgID), nil)
	if err != nil {
		return err
	}

	body, err := c.doRequest(req, authToken)
	if err != nil {
		return err
	}

	if string(body) != "Deleted org" {
		return errors.New(string(body))
	}

	return nil
}
